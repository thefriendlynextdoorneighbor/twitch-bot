import envConfig from 'dotenv'
import tmi from 'tmi.js'
import discord from 'discord.js'
import { db, addScheduledPoints } from './src/db'
import {
    connected_handling,
    join_handling,
    part_handling,
    message_handling,
    whisper_handling,
} from './src/event_handling'
import twitch_api from './src/twitch_api'
import { TWITCH_USER } from './src/types'

// Set twitch bot options
envConfig.config() // Retrieve .env variables
const CHANNELS: string[] = (process.env.CHANNELS as string).split(", ")
const TWITCH_BOT_OPTIONS = {
    identity: {
        username: process.env.BOT_NICK,
        password: process.env.TMI_TOKEN,
    },
    channels: CHANNELS,
}

// Create twitch bot
const twitchBot: tmi.Client = (tmi.client(TWITCH_BOT_OPTIONS) as tmi.Client)

// Create discord bot
const discordBot: discord.Client =  new discord.Client()

// Store refs
const REFS = {
    twitchBot,
    discordBot,
    db,
    api: new twitch_api({
        "Client-Secret": (process.env.CLIENT_SECRET as string),
        "Client-ID": (process.env.CLIENT_ID as string),
    }),
}

// Twitch bot Behavior Assignment
twitchBot.on('connected', (address, port) => connected_handling(REFS, address, port))
twitchBot.on('join', (channel, username, self) => join_handling(REFS, channel, username, self))
twitchBot.on('part', (channel, username, self) => part_handling(REFS, channel, username, self))
twitchBot.on('message', (channel, context, message, self) => message_handling(REFS, channel, context, message, self))
twitchBot.on('whisper', (from, context, message, self) => whisper_handling(REFS, from, context, message, self))

// Connect to Twitch
twitchBot.connect()

// Schedule point dist
function schedulePointDrop (): void {
    setTimeout(() => {
        TWITCH_BOT_OPTIONS.channels.forEach(async (channel) => {
            const stream = await REFS.api.getStream(channel.substr(1))
            if (stream && stream.length > 0) {
                const currViewers = await REFS.api.getUsersInStream(channel.substr(1))
                if (currViewers && currViewers.length > 0) {
                    const currViewerIDs = await REFS.api.getUsersByLogin(currViewers as string[])
                    if (currViewerIDs) addScheduledPoints(REFS.db, currViewerIDs as TWITCH_USER[])
                }
            }
        })
        schedulePointDrop()
    }, 60 * 1000)
}

schedulePointDrop()


// Discord bot Behavior Assignment
discordBot.on('ready', () => {
    console.log(`Logged in as ${(discordBot.user as discord.User).tag}!`)
})
discordBot.on('message', msg => {
    if (msg.content === 'ping') {
        msg.reply('pong')
    }
})

// Connect to Discord
discordBot.login(process.env.DISCORD_TOKEN)
