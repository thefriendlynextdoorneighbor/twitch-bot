# Twitch Bot
This twitch bot is a personal project intended to link multiple twitch channels for my friends and I; however it could be modified to be used by others.

# Setup
Use npm to install necessary dependencies
```$ npm install```

Configure the bot to your settings using the `.env` file, it should look something like this.
```TMI_TOKEN=oauth:********************
CLIENT_ID=*****************
BOT_NICK="MyBot"
BOT_PREFIX="!"
CHANNELS="channel1, channel2"
```

You will also need to install a sqlite database and name it TwitchDB.db stored in the `./sqlite` subdirectory

# Running
Run the bash script to compile and run the bot
```$ ./run```


