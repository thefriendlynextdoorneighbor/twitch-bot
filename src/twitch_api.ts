import { TWITCH_AUTH, TWITCH_USER, TWITCH_CHATTER } from './types'
import { setLongTimeout } from './util'
import axios from 'axios'

export default class TwitchApi {
    opts: TWITCH_AUTH;
    accessToken: string | boolean = false;
    hasAccess = false;

    url = "https://api.twitch.tv/helix";

    constructor (defaultOpts: TWITCH_AUTH) {
        this.opts = defaultOpts
        this.getAccessToken()
    }

    private async getAccessToken (): Promise<void> {
        console.assert('^ Retrieving Twitch Api Access Token')
        try {
            const { data: resp }: { data: { access_token: string, expires_in: number } } = await axios.post(
                `https://id.twitch.tv/oauth2/token?client_id=${this.opts['Client-ID']}&client_secret=${this.opts['Client-Secret']}&grant_type=client_credentials&scope=user_read`,
            )
            this.accessToken = resp.access_token
            this.hasAccess = true
            console.log('^ Twitch Api Access Token Aquired!')
            // Retrieve new token before expiration
            setLongTimeout(() => { this.hasAccess = false; console.warn('^ Access Token has Expired') }, resp.expires_in * 1000)
            setLongTimeout(() => this.getAccessToken, resp.expires_in * 1000)
        } catch (err) {
            console.error('^ Failed to Retrieve Twitch Api Access Token')
            console.error(err)
            setLongTimeout(this.getAccessToken, 20 * 1000)
        }
    }

    private getCreds () {
        return {
            'Client-ID': this.opts['Client-ID'],
            'Authorization': `Bearer ${this.accessToken}`,
        }
    }

    public async getUsersByID (ids: string[]): Promise<TWITCH_USER[] | null>  {
        if (this.hasAccess) {
            try {
                const { data: { data: resp } }: { data: { data: TWITCH_USER[] } } = await axios.get(`${this.url}/users?${ids.map(login => `id=${login}`).join('&')}`, {
                    headers: { 
                        ...this.getCreds(),
                        'Content-Type': 'application/json',
                    },
                })
                return resp
            } catch (err) { console.log(err) }
        } else { console.warn('^ No Access Token Yet') }
        return null
    }

    public async getUsersByLogin (logins: string[] ): Promise<TWITCH_USER[] | null> {
        if (this.hasAccess) {
            try {
                const { data: { data: resp } }: { data: { data: TWITCH_USER[] } } = await axios.get(`${this.url}/users?${logins.map(login => `login=${login}`).join('&')}`, {
                    headers: { 
                        ...this.getCreds(),
                        'Content-Type': 'application/json',
                    },
                })
                return resp
            } catch (err) { console.log(err) }
        } else { console.warn('^ No Access Token Yet') }
        return null
    }

    public async getUsersInStream (channel: string): Promise<string[] | null> {
        if (this.hasAccess) {
            try {
                const { data: { chatters } }: { data: TWITCH_CHATTER } = await axios.get(`https://tmi.twitch.tv/group/user/${channel}/chatters`, {
                    headers: { 
                        ...this.getCreds(),
                        'Content-Type': 'application/json',
                    },
                })
                let chatterList = chatters.viewers.concat(chatters.vips)
                chatterList = chatterList.concat(chatters.broadcaster)
                chatterList = chatterList.concat(chatters.moderators)
                chatterList = chatterList.concat(chatters.staff)
                chatterList = chatterList.concat(chatters.admins)
                chatterList = chatterList.concat(chatters.global_mods)
                return chatterList
            } catch (err) { console.warn(`^ Error retrieving users: ${err}`) }
        }
        return null
    }

    public async getStream (channel: string): Promise<any[] | null>  {
        if (this.hasAccess && channel) {
            try {
                const resp: { data: { data: any[], pagination: unknown } } = await axios.get(`${this.url}/streams?user_login=${channel}`, {
                    headers: {
                        ...this.getCreds(),
                        'Content-Type': 'application/json',
                    },
                })
                return resp.data.data
            } catch (err) { console.warn(`^ Error retrieving stream: ${err}`) }
        }
        return null
    }
}