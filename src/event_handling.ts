import tmi from 'tmi.js'
import { REFS_TYPE, TWITCH_USER } from './types'
import { storeUser, getUserPoints } from './db'

// Bot Behavior
export function connected_handling({}: REFS_TYPE, addr: string, port: number): void {
    console.log(`* Connected to ${addr}:${port}`)
}

export async function join_handling({ api, db }: REFS_TYPE, channel: string, username: string, self: boolean): Promise<void> {
    if (!self) console.log(`* ${username} joined ${channel}`)
    // twitchBot.say(channel, `Welcome to the stream ${username}`)
    const resp = await api.getUsersByLogin([username])
    const userData = !!resp ? resp[0] : null
    storeUser(db, (userData as unknown as TWITCH_USER))
}

export function part_handling({}: REFS_TYPE, channel: string, username: string, self: boolean): void {
    if (!self) console.log(`* ${username} parted ${channel}`)
}

export async function message_handling({ db, twitchBot, api, discordBot }: REFS_TYPE, channel: string, context: tmi.ChatUserstate, message: string, self: boolean): Promise<void> {
    if (!self) {
        console.log(`**${channel}:${context['display-name']} ${message}`)
        commonCommands({ db, twitchBot, api, discordBot }, channel, context, message, self)
    }
}

export function whisper_handling({ twitchBot }: REFS_TYPE, from: string, context: tmi.ChatUserstate, message: string, self: boolean): void {
    if (!self) {
        console.log(`**WHISPER:${context['display-name']} ${message}`)
    }
}

async function commonCommands ({ db, twitchBot, api }: REFS_TYPE, from: string, context: tmi.ChatUserstate, message: string, self: boolean) {
    if (message === "!test") twitchBot.whisper(from, "I hear you!")
    if (message === "!score") {
        const resp = await api.getUsersByLogin([context['username'] as string])
        if (resp && resp.length > 0) {
            getUserPoints(db, resp[0], (score: number) => {
                twitchBot.whisper(context['username'] as string, score ? `You currently have ${score}` : `Error retrieving score`)
            })
        }
    }
}