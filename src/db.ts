import sqlite3 from 'sqlite3'
import { TWITCH_USER } from './types'

export const db: sqlite3.Database = new sqlite3.Database('sqlite/TwitchDB.db', (err: Error | null) => {
    if (err) return console.error(`$ Database could not connect: ${err.message}`)
    return console.log('$ Successful connection to the database')
})

export async function storeUser(db: sqlite3.Database, userData: TWITCH_USER): Promise<void> {
    if (userData) {
        try {
            db.get('SELECT twitch_id TwitchID FROM users WHERE twitch_id = ?', [(userData as unknown as TWITCH_USER).id], (err, row) => {
                if (err) console.warn(`$ Error retrieving user: ${err}`)
                if (row === undefined)  {
                    db.run('INSERT INTO users(twitch_id, name) VALUES(?, ?)', [userData.id, userData.login], (err) => { if (err) console.warn(`$ Error storing user: ${err}`) })
                }
            })
        } catch (err) {console.warn(err)}
    }
}

export async function addScheduledPoints (db: sqlite3.Database, users: TWITCH_USER[]): Promise<void> {
    console.log(`$ awarded points to ${users.length} users`)
    users.forEach((userData) => {
        db.run('UPDATE users SET score = score + 1 WHERE twitch_id = ?', [userData.id], (err) => { if (err) console.warn(`$ Error storing user score: ${err}`) })
    })
    
}

export async function getUserPoints(db: sqlite3.Database, userData: TWITCH_USER, callback: (score: number) => void ): Promise<void> {
    db.get('SELECT score FROM users WHERE twitch_id = ?', [userData.id], (err, row: { score: number }) => {
        if (err) console.warn(`$ Error retrieving user score: ${err}`)
        if (row) callback(row.score)
    })
}