const MAX_TIMEOUT = Math.pow(2, 31) - 1

export function setLongTimeout(callback: () => unknown, timeout: number): void {
    if (timeout <= MAX_TIMEOUT) {
        setTimeout(callback, timeout)
    } else {
        setTimeout(() => { setLongTimeout(callback, (timeout - MAX_TIMEOUT)) }, MAX_TIMEOUT)
    }
}