import tmi from 'tmi.js'
import discord from 'discord.js'
import sqlite3 from 'sqlite3'
import TwitchApi from './twitch_api'

export type TWITCH_AUTH = { "Client-Secret": string, "Client-ID": string }

export type REFS_TYPE = { twitchBot: tmi.Client, db: sqlite3.Database, api: TwitchApi, discordBot: discord.Client }

export type TWITCH_USER = {
    id: string,
    login: string,
    display_name: string,
    type: string,
    description: string,
    profile_image_url: string,
    offline_image_url: string,
    view_count: number,
}

export type TWITCH_CHATTER = {
    chatter_count: number,
    chatters: {
        broadcaster: string[],
        vips: string[],
        moderators: string[],
        staff: string[],
        admins: string[],
        global_mods: string[],
        viewers: string[]
    }
}